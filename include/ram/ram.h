#ifndef RAM_C

#pragma once
#include <stdint.h> 
#include <stdlib.h> 


typedef struct { 

    uint8_t* ram; 

} Chip8Ram; 

  

// Initialisation de la mémoire 

Chip8Ram initMemoire();

  

// Libération de la mémoire 

void LibMemoire(Chip8Ram *chip8Ram);

  

// Fonction pour lire un octet depuis la mémoire à une adresse donnée 

uint8_t lireMemoire(const Chip8Ram *chip8Ram, uint16_t addresse);

  

// Fonction pour écrire un octet dans la mémoire à une adresse donnée 

void ecrireMemoire(Chip8Ram *chip8Ram, uint16_t addresse, uint8_t valeur);

#endif
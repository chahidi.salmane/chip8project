#ifndef PROCESSEUR_H

#pragma once
#include <stdint.h> 
#include <stdlib.h> 
#include "include/ram/ram.h"

typedef struct {
uint16_t *stack;
	uint8_t tempRetard;
	uint8_t	tempSon;
	Chip8Ram * ram;
	uint16_t I;
	uint8_t *V;
	uint16_t pc;
	uint8_t sp;
}Chip8Processeur;

Chip8Processeur initProcesseur(Chip8Ram *ram);




#endif
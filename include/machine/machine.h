#include "include/ram/ram.h"
#include "include/processor/processeur.h"
#include "include/display/display.h"
#include "include/display/sprite.h"
#include "include/keyboard/keyboard.h"
#include "include/misc/error.h"
#include "include/speaker/speaker.h"
#include <stdio.h>

typedef struct {
	Sprite sprite;
	Chip8Processeur processeur;
	Keyboard keyboard;
	Chip8Ram ram;
}Machine;

void Machine_init(Machine* machine);

void Machine_loop(Machine* machine);

int Machine_load(Machine* machine, const char* argv);

void Machine_destroy(Machine* machine);
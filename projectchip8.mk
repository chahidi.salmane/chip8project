##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=projectchip8
ConfigurationName      :=Debug
WorkspacePath          :=/home/administrateur/Documents/projet
ProjectPath            :=/home/administrateur/Documents/projet/projectchip8
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=administrateur
Date                   :=24/01/24
CodeLitePath           :=/home/administrateur/.codelite
LinkerName             :=gcc
SharedObjectLinkerName :=gcc -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="projectchip8.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -lprovided -lSDL2
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)lib/debug/x64 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := gcc
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IntermediateDirectory)/scripts_machine.c$(ObjectSuffix) $(IntermediateDirectory)/scripts_processeur.c$(ObjectSuffix) $(IntermediateDirectory)/scripts_ram.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.c$(ObjectSuffix): main.c $(IntermediateDirectory)/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/administrateur/Documents/projet/projectchip8/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main.c$(DependSuffix) -MM main.c

$(IntermediateDirectory)/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.c$(PreprocessSuffix) main.c

$(IntermediateDirectory)/scripts_machine.c$(ObjectSuffix): scripts/machine.c $(IntermediateDirectory)/scripts_machine.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/administrateur/Documents/projet/projectchip8/scripts/machine.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/scripts_machine.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/scripts_machine.c$(DependSuffix): scripts/machine.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/scripts_machine.c$(ObjectSuffix) -MF$(IntermediateDirectory)/scripts_machine.c$(DependSuffix) -MM scripts/machine.c

$(IntermediateDirectory)/scripts_machine.c$(PreprocessSuffix): scripts/machine.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/scripts_machine.c$(PreprocessSuffix) scripts/machine.c

$(IntermediateDirectory)/scripts_processeur.c$(ObjectSuffix): scripts/processeur.c $(IntermediateDirectory)/scripts_processeur.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/administrateur/Documents/projet/projectchip8/scripts/processeur.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/scripts_processeur.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/scripts_processeur.c$(DependSuffix): scripts/processeur.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/scripts_processeur.c$(ObjectSuffix) -MF$(IntermediateDirectory)/scripts_processeur.c$(DependSuffix) -MM scripts/processeur.c

$(IntermediateDirectory)/scripts_processeur.c$(PreprocessSuffix): scripts/processeur.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/scripts_processeur.c$(PreprocessSuffix) scripts/processeur.c

$(IntermediateDirectory)/scripts_ram.c$(ObjectSuffix): scripts/ram.c $(IntermediateDirectory)/scripts_ram.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/administrateur/Documents/projet/projectchip8/scripts/ram.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/scripts_ram.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/scripts_ram.c$(DependSuffix): scripts/ram.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/scripts_ram.c$(ObjectSuffix) -MF$(IntermediateDirectory)/scripts_ram.c$(DependSuffix) -MM scripts/ram.c

$(IntermediateDirectory)/scripts_ram.c$(PreprocessSuffix): scripts/ram.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/scripts_ram.c$(PreprocessSuffix) scripts/ram.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/



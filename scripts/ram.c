#include "include/ram/ram.h"


// Initialisation de la mémoire 

Chip8Ram initMemoire() { 

    Chip8Ram chip8Ram; 

    chip8Ram.ram = (uint8_t*)malloc(4096 * sizeof(uint8_t)); 

    if (chip8Ram.ram == NULL) { 

        // Gestion d'une erreur d'allocation mémoire 

        exit(EXIT_FAILURE); 

    } 

    return chip8Ram; 

} 

  

// Libération de la mémoire 

void LibMemoire(Chip8Ram *chip8Ram) { 

    free(chip8Ram->ram); 

} 

// Fonction pour lire un octet depuis la mémoire à une adresse donnée 

uint8_t lireMemoire(const Chip8Ram *chip8Ram, uint16_t addresse) { 

    if (addresse < 4096) { 

        return *(chip8Ram->ram + addresse); 

    } else { 

        // Gérer l'accès hors limites (dans un véritable émulateur, vous voudrez peut-être prendre des mesures appropriées) 

        return 0; 

    } 

} 

  

// Fonction pour écrire un octet dans la mémoire à une adresse donnée 

void ecrireMemoire(Chip8Ram *chip8Ram, uint16_t addresse, uint8_t valeur) { 

    if (addresse < 4096) { 

        *(chip8Ram->ram + addresse) = valeur; 

    } else { 

        // Gérer l'accès hors limites (dans un véritable émulateur, vous voudrez peut-être prendre des mesures appropriées) 

    } 

} 
#include "include/processor/processeur.h"



Chip8Processeur initProcesseur(Chip8Ram *ram){
	Chip8Processeur processeur;
	processeur.V = (uint8_t*)malloc(16*sizeof(uint8_t));
    for (int i = 0; i < 16; ++i) {
        processeur.V[i] = 0;
    }
    
	processeur.stack = (uint16_t*)malloc(16*sizeof(uint16_t));
	processeur.tempRetard = 0;
	processeur.tempSon = 0;
	processeur.I = 0;
	processeur.pc = 0x200;
	processeur.sp = 0;
	processeur.ram = ram;
	
	return processeur;
}

void cleanupProcessor(Chip8Processeur* processeur) {
    free(processeur->V);  // Free allocated memory for the registers
}

/*uint16_t fetchInstruction(Chip8Processeur* processeur) {
    uint16_t instruction = (processeur->ram[processeur->pc] << 8) | processor->ram[processor->pc + 1];
    processeur->pc += 2;
    return instruction;
}*/

void interpretAndExecute(Chip8Processeur* processeur, uint16_t instruction) {
    uint16_t opcode = instruction >> 12;
    uint8_t x = (instruction >> 8) & 0x0F;
    uint8_t y = (instruction >> 4) & 0x0F;
    uint8_t kk = instruction & 0xFF;
    uint16_t nnn = instruction & 0xFFF;
    
    switch (opcode) {
        case 0x0:
            if (instruction == 0x00E0) {
            } else if (instruction == 0x00EE) {
                processeur->pc = processeur->stack[--processeur->sp];
            }
            break;

        case 0x1:
            processeur->pc = nnn;
            break;

        case 0x2:
            processeur->stack[processeur->sp++] = processeur->pc;
            processeur->pc = nnn;
            break;

        case 0x3:
            if (processeur->V[x] == kk) {
                processeur->pc += 2;
            }
            break;
			
		case 0x4:
			if(processeur->V[x] != kk)
				processeur->pc += 2;
			break;
		
		case 0x5:
			if(processeur->V[x] == processeur->V[y])
				processeur->pc += 2;
			break;

        case 0x6:
            // LD Vx, byte - Set Vx = kk
            processeur->V[x] = kk;
            break;

		case 0x7:
			processeur->V[x] += kk;
			break;
		
		/*case 0x8:
			if(instruction == 8xy0)
				processeur->V[x] = processeur->V[y];
			else if(instruction == 8xy1)
				processeur->V[x] = (processeur->V[x] || processeur->V[y]);//TODO: à corriger*/
			//else if(instruction == 
				
		
        case 0xA:
            // LD I, addr - Set I = nnn
            processeur->I = nnn;
            break;
			
		case 0xD:
			

        default:
            break;
    }
}
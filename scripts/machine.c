#include "include/machine/machine.h"


void Machine_init(Machine* machine){
	if(Keyboard_init(&machine->keyboard)==1){
		fprintf(stderr, "malloc out of memory\n");
	}
	machine->ram = initMemoire();
	machine->processeur = initProcesseur(&machine->ram);
}

void Machine_loop(Machine* machine){
	
}

int Machine_load(Machine* machine, const char* romPath) {
    FILE* romFile = fopen(romPath, "rb");
    if (!romFile) {
        fprintf(stderr, "Error opening ROM file: %s\n", romPath);
        return 0;
    }

    fseek(romFile, 0, SEEK_END);
    long romSize = ftell(romFile);
    fseek(romFile, 0, SEEK_SET);

    if (romSize > sizeof(machine->ram) - 0x200) {
        fprintf(stderr, "Error: ROM is too large to fit in memory.\n");
        fclose(romFile);
        return 0;
    }

    // 0x200
    fread(&machine->ram, 1, romSize, romFile);

    fclose(romFile);
    return 1;  
}
void Machine_destroy(Machine* machine){
	LibMemoire(&machine->ram);
    cleanupProcessor(&machine->processeur);
    Keyboard_destroy(&machine->keyboard);
    Sprite_destroy(&machine->sprite);
}
#include "include/ram/ram.h"
#include "include/processor/processeur.h"
#include "include/display/display.h"
#include "include/display/sprite.h"
#include "include/keyboard/keyboard.h"
#include "include/misc/error.h"
#include "include/speaker/speaker.h"
#include <stdio.h>
//sudo apt-get install libsdl2-dev

int main(int argc, char **argv)
{
	if (argc != 2) {
                fprintf(stderr, "Unexpected number of arguments.\n");
                fprintf(stderr, "Usage: emulator <ROM.ch8>\n");
                return 1;
        }
	// Initialisation de la mémoire à la taille maximum que le programme pourra y accéder 4KB de RAM
	Chip8Ram chip8Ram = initMemoire();
	Chip8Processeur chip8Processeur = initProcesseur(&chip8Ram);
	
	// Notre programme Chip-8 commence à l’emplacement 0x200 (512)
	uint16_t adresse = 0x200;
	
	// Teste de la fonction lire mémoire 
	uint8_t valeur = lireMemoire(&chip8Ram, adresse);
	
	// Teste de la function ecriture mémoire
	ecrireMemoire(&chip8Ram, adresse, valeur);
	LibMemoire(&chip8Ram);
	
	/*if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER|SDL_INIT_AUDIO)) {
                errcode = SDL;
        }
	else {
			printf("The SDL library has been initialized successfully.\n");
			struct Machine machine;
			if (!Machine_init(&machine)) {
					printf("The CHIP 8 emulator is ready.\n");
					if (!Machine_load(&machine,argv[1])) {
							printf("The ROM <%s> has been loaded.\n",argv[1]);
							printf("Run...\n");
							while (
									!Machine_loop(&machine)
							);
					}
					Machine_destroy(&machine);
			}
			SDL_Quit();
	}
	if (errcode != QUIT) {
		fprintf(stderr,"The program has terminated abnormally (errcode=%s)\n",errorstr());
		return 1;
	
	}*/
	printf("hello world\n");
	return 0;
}
